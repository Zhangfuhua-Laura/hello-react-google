import React from 'react';
import './App.less';
import Menu from "./Menu";
import Search from './Search';

const App = () => {
  return (
    <div className='App'>
      <Menu />
      <Search />
    </div>
  );
};

export default App;